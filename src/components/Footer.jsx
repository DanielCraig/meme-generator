import React from 'react'

function Footer() {
    return (
        <footer>© 2019 - Alessandro Camplese</footer>
    )
}

export default Footer